import os
import re
import tomllib
import json
import subprocess

with open("../swim/Cargo.toml", "rb") as f:
    data = tomllib.load(f)
    swim_version_raw = data["package"]["version"]

swim_git_rev_list_raw = subprocess.check_output([
    "git", "rev-list", "HEAD"
], encoding="utf-8", cwd="../swim").split()
local_git_rev_list_raw = subprocess.check_output([
    "git", "rev-list", "HEAD"
], encoding="utf-8").split()


swim_version = re.match(r"^(\d+)\.(\d+).(\d+)?$", swim_version_raw)
swim_major  = int(swim_version[1])
swim_minor  = int(swim_version[2])
swim_patch = int(swim_version[3])

swim_commit = swim_git_rev_list_raw[0]
local_commit = local_git_rev_list_raw[0]

version = f"{swim_major}.{swim_minor + 1}.{swim_patch}-dev{len(swim_git_rev_list_raw)}.{len(local_git_rev_list_raw)}.{swim_commit[0:6]}-{local_commit[0:6]}"
print(f"version {version}")

with open("package-in.json", "rt") as f:
    package_json = json.load(f)
package_json["version"] = version
with open("package.json", "wt") as f:
    json.dump(package_json, f, indent=2)

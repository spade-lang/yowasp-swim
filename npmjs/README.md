YoWASP Swim package
====================

This package provides [Swim][] binaries built for [WebAssembly][]. This is using the yowasp project to do the heavy lifting. See the [overview of the YoWASP project][yowasp] for details.

At the moment, this package only provides an API allowing to run Swim in a virtual filesystem; no binaries are provided.

[Swim]: https://gitlab.com/spade-lang/swim/
[webassembly]: https://webassembly.org/
[yowasp]: https://yowasp.github.io/


API reference
-------------

This package provides two functions:

- `runSwim`
- `runSwimPrepare`

`runSwim` behaves like a desktop swim, except it doesn't download spade or
libraries. `runSwimPrepare` downloads the libraries specified swim.toml in in
order to make `swim build` behave well despite not being able to download
libraries due to wasi limitations.

For more detail, see the documentation for [the JavaScript YoWASP runtime](https://github.com/YoWASP/runtime-js#api-reference).


Versioning
----------

The version of this package is derived from the upstream Swim package version in the `X.Y.Z-devH` format, and can be in one of the two formats, `X.Y.M` (for builds from release branches) or `X.(Y+1).N-dev.M` (for development builds), where the symbols are:

1. `X`: Swim major version
2. `Y`: Swim minor version
3. `Z`: Swim patch version; ignored if present
3. `H`: The commit hash which Swim was built from

With this scheme, there is a direct correspondence between upstream versions and [SemVer][semver] NPM package versions.

Note that for development builds, the minor version is incremented as required by the SemVer comparison order. This means that an NPM package built from the upstream version `0.45+12` and the 120th commit in this repository will have the version `0.46.12-dev.120`. Because this is a pre-release package, it will not be matched by version specifiers such as `^0.46` or `>=0.46`.

[semver]: https://semver.org/

Credit
------

All of the heavy lifting here is done by  Catherine "whitequark" via the https://github.com/YoWASP project.


License
-------

This package and the Swim compiler is covered by the [EUPL-1.2](LICENSE.txt).  The YoWASP build flow used to bundle Swim in JavaScript is originally licensed under the [ISC](LICENSE-ISC-whitequark.txt)

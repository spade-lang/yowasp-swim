import { Application } from '@yowasp/runtime';
import { instantiate } from '../gen/swim.js';
import { runSwimPrepare } from '../gen/swimPrepare.js';
import { HttpRequest, makeHttpRequest } from './makeHttpRequest.js';

export { Exit } from '@yowasp/runtime';


let swim = new Application(() => import('./resources-swim.js'), instantiate, 'yowasp-swim');
swim.extraIncludes = {swimUtil: {makeHttpRequest: makeHttpRequest, HttpRequest: HttpRequest}}

const runSwim = swim.run.bind(swim);


export { runSwim, runSwimPrepare };
export const commands = { 'swim': runSwim };

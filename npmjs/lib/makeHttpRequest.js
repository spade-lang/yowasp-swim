
export class HttpRequest {
    constructor(url_) {
        this.result = null
        // Gitlab annoyingly doesn't allow us to download tar files from another page
        // because CORS 😠. However, we can just use a proxy, apparently.
        // const url = 'https://corsproxy.io/?' + encodeURIComponent(url_);
        const url = 'https://corsproxy.io/?https://example.com';

        const req = new XMLHttpRequest();
        req.open("GET", url, true);
        req.setRequestHeader("Accept", "*/*")

        req.onload = (event) => {
            console.log("onload trigger")
            const arrayBuffer = req.response; // Note: not req.responseText
            if (arrayBuffer) {
                this.result = arrayBuffer;
            }
            else {
                this.result = "DUMMY"
            }
        };
        req.onerror = (event) => {
            console.log("Error")
            this.result = "DUMMY"
        }
        req.onabort = (event) => {
            console.log("Error")
            this.result = "DUMMY"
        }
        req.onprogress = (event) => {
            console.log(event)
        }

        req.send(null);

        console.log("Request sent");
    }

    done() {
        return this.result;
    }
}

export function makeHttpRequest(url_) {
    console.log("Starting http request");

}


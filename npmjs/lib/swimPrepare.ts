export {runSwimPrepare}

import { RunOptions, Tree, runSwim } from "./api"

async function runSwimPrepare(args: string[], filesIn: Tree, config: RunOptions) : Promise<any> {
    if (args.length != 0) {
        throw Error(`swim-prepare does not take any argument. Was passed ${args}`)
    }
    runSwim(["prepare"], filesIn, config)
}


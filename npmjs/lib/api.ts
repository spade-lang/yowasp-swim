import { Application, Exit, RunOptions, Tree } from '@yowasp/runtime';
import { instantiate } from '../gen/swim.js';
// import { runSwimPrepare } from '../gen/swimPrepare.js';
import { HttpRequest, makeHttpRequest } from './makeHttpRequest.js';

export { Exit } from '@yowasp/runtime';

class Request {
    url: string
    file: string
}

async function create_dir(tree: Tree, dir: string[]) {
    if (dir.length == 0) {
        return
    }

    if (!(dir[0] in tree)) {
        tree[dir[0]] = {}
    }
    const subtree = tree[dir[0]] as Tree
    if (subtree !== undefined) {
        create_dir(subtree, dir.slice(1))
    }
    else {
        throw Error(`${dir[0]} was a file not a dir`)
    };
}

async function put_file_at(tree: Tree, dir: string[], file: Uint8Array) {
    if (dir.length == 0) {
        throw Error(`Attempting to put file at empty file name`)
    }
    else if (dir.length == 1) {
        tree[dir[0]] = file
    }
    else {
        const subtree = tree[dir[0]] as Tree
        if (subtree !== undefined) {
            put_file_at(subtree, dir.slice(1), file)
        }
        else {
            throw Error(`${dir[0]} was a file not a dir`)
        };
    }
}

async function runSwimPrepare(args: string[], filesIn: Tree, config: RunOptions) : Promise<any> {
    let files = filesIn;

    const onError = (message: string) => {
        if (config.stderr !== null) {
            config.stderr(new TextEncoder().encode(message + "\n"))
            let err = new Exit();
            err.code = 1;
            err.files = files;
            throw err;
        }
    };

    if (args.length != 0) {
        onError(`swim-prepare does not take any argument. Was passed ${args}`)
    }
    let done = false;
    while (!done) {
        files = await runSwim(["prepare-libraries"], files, config)

        try {
            const to_fetch_content = files["build"]["libs_to_fetch"];

            let requested = null
            try {
                let to_fetch_content_str = to_fetch_content;
                if (typeof to_fetch_content_str !== "string") {
                    const dec = new TextDecoder()
                    to_fetch_content_str = dec.decode(to_fetch_content)
                }
                requested = JSON.parse(to_fetch_content_str) as Request[];
            } catch (e) {
                onError(`Failed to decode build/libs_to_fetch\n    ${e}`)
            }

            for (let req of requested) {
                const url = req.url;
                const response = await fetch(url)

                if (response.status != 200) {
                    onError(`Failed to fetch ${url}. Status ${response.status} (${response.statusText})`)
                }

                const content = await response.arrayBuffer().then(buffer => {
                    return new Uint8Array(buffer)
                });

                console.log(content.length, content)

                // Paths are absolute here, so swim will emit a "/"
                const dest = req.file.split("/").slice(1);
                create_dir(files, dest.slice(0, -1))
                put_file_at(files, dest, content);
            }

            done = requested.length == 0
        } catch (e) {
            onError(`${e}`)
        }
    }
    return files;
}



let swim = new Application(() => import('./resources-swim.js'), instantiate, 'yowasp-swim');

const runSwim = swim.run.bind(swim);


export { runSwim, runSwimPrepare };
export const commands = { 'swim': runSwim, 'swimPrepare': runSwimPrepare };

YoWASP Swim Package
=====================

This package provides [Swim][] binaries built for [WebAssembly][]. This is using the yowasp project to do the heavy lifting. See the [overview of the YoWASP project][yowasp] for details.

For more details, see the [JavaScript](npmjs/README.md) readme.

[Swim]: https://gitlab.com/spade-lang/swim/
[webassembly]: https://webassembly.org/
[yowasp]: https://yowasp.github.io/



Credit
------

All of the heavy lifting here is done by  Catherine "whitequark" via the [yowasp][] project.


License
-------

This package and the Swim compiler is covered by the [EUPL-1.2](LICENSE.txt). The YoWASP build flow used to bundle Swim in JavaScript is originally licensed under the [ISC](LICENSE-ISC-whitequark.txt)
